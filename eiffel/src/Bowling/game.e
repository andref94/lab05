note
	description: "bowl application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	GAME

inherit
	ARGUMENTS

create
	make

feature {ANY}

	totalScore: INTEGER
	frame: INTEGER
	points: ARRAY [INTEGER]
	strike: INTEGER

	roll(pins: INTEGER) do
		points.put(pins,frame)

		if frame <= 19 then

			if strike > 0 and frame < 19 then
				totalscore := totalscore + pins
				strike := strike - 1
			end
			if pins = 10 and frame < 19 and frame > 1 then
				strike := strike + 1
			end
			if frame >= 2 then
				if points.item(frame-2) = 10 and frame\\2 = 0 then
					totalScore := totalScore + pins
				else
					if frame >= 3 and points.item(frame-3) = 10 and frame\\2 = 1 then
						totalscore := totalscore + pins
					else
						if frame\\2 = 0 and points.item(frame-2)+points.item(frame-1) = 10 then
							totalscore := totalscore + pins
						end
					end
				end
				totalscore := totalscore + pins
			else
				totalscore := totalscore + pins
			end
			print(totalscore)
			print(", ")
			if pins = 10 then
				frame := frame + 2
			else
				frame := frame + 1
			end

		else

			if frame = 20 then
				if points.item (18) = 10 then
					totalscore := totalscore + pins + pins
					frame := frame + 1
					print(totalscore)
					print(", ")
				else if points.item (18) + points.item (19) = 10 then
					totalscore := totalscore + pins
				end
				end
			else
				if points.item(18) = 10 then
					totalscore := totalscore + pins
					print(totalscore)
				end
			end

		end

	end

	score: INTEGER do
		Result := totalScore
	end


feature {NONE} -- Initialization

	make
			-- Run application.
		do
			--| Add your code here
			totalScore := 0
			frame := 0
			strike := 0
			!! points.make(0,22)
		end

end
