note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "EiffelStudio test wizard"
	date: "$Date$"
	revision: "$Revision$"
	testing: "type/manual"

class
	GAME_TEST_SET

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end

feature {NONE} -- Events

	g: GAME

	on_prepare
			-- <Precursor>
		do
			create g.make
		end

feature -- Test routines

	testGutterGame
			-- testGutterGame
		do
			across 1 |..| 20 as i loop
				g.roll(0)
			end
			assert ("Gutter game not zero", g.score = 0)
		end

	testAllOnes
			-- testAllOnes
		do
			across 1 |..| 20 as i loop
				g.roll(1)
			end
			assert ("All ones test isn't 20", g.score = 20)
		end

	testOneSpare
			-- testOneSpare
		do
			g.roll (3)
			g.roll (7)
			g.roll (3)
			across 1 |..| 17 as i loop
				g.roll (0)
			end
			assert ("Test one spare isn't correct", g.score = 16)
		end

	testOneStrike
			-- testOneStrike
		do
			g.roll (10)
			g.roll (3)
			g.roll (4)
			across 1 |..| 16 as i loop
				g.roll (0)
			end
			assert ("Test one strike isn't correct", g.score = 24)
		end

	testPerfectGame
			-- testPerfectGame
		do
			across 1 |..| 12 as i loop
				g.roll(10)
			end
			assert ("You don't made a perfect game", g.score = 300)
		end

	testLastSpare
			-- testLastSpare
		do
			across 1 |..| 9 as i loop
				g.roll(10)
			end
			g.roll (5)
			g.roll (5)
			g.roll (10)
			assert ("You don't made a last spare", g.score = 275)
		end

end
